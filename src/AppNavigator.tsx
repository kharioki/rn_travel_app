import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
// import { createStackNavigator } from '@react-navigation/stack';
import { FlightsList, TravelRequest, Welcome } from './screens';
import * as Route from './Routes';

/**
 * rn-macos: 'native-stack' doesn't work as that contains native code, so use 'stack' which comes with javascript based implementation for navigation
 * rn-windows: 'native-stack' crashes when navigating back, and 'stack' throws some "folly::toJson" error, so both kind of work
 * For others using 'native-stack'
 */
const Stack = createNativeStackNavigator<Route.RootStackParamList>();
// const Stack = createStackNavigator<Route.RootStackParamList>();

const AppNavigator = () => {
  const linking = {
    prefixes: ['https://flightbooking.com', 'flightbooking://'],
    config: {
      screens: {
        [Route.TRAVEL_REQUEST]: Route.TRAVEL_REQUEST,
        [Route.FLIGHTS_LIST]: Route.FLIGHTS_LIST,
      },
    },
  };

  return (
    <NavigationContainer linking={linking}>
      <Stack.Navigator screenOptions={{ headerShown: false }}>
        <Stack.Screen
          name={Route.WELCOME_DESKTOP}
          component={Welcome.Desktop}
        />
        <Stack.Screen name={Route.WELCOME_MOBILE} component={Welcome.Mobile} />
        <Stack.Screen name={Route.TRAVEL_REQUEST} component={TravelRequest} />
        <Stack.Screen name={Route.FLIGHTS_LIST} component={FlightsList} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default AppNavigator;
