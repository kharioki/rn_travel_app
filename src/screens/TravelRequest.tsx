import React, { useState } from 'react';
import {
  Image,
  KeyboardAvoidingView,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  View,
  Platform,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import FlightItem from './ui/flightItem/FlightItem';
import TravelDetails from './ui/travelDetail/TravelDetails';
import Button from './components/Button';
import { Alert } from './components/Alert.web';
import { RootState, updateNotes } from '../store';
import { Config } from '../config';
import { AppImages } from '../assets';

const TravelRequest = () => {
  const insets = useSafeAreaInsets();

  const travelState = useSelector((state: RootState) => state.travel_request);
  const dispatch = useDispatch();

  const [screenHeight, setScreenHeight] = useState(0);

  const onProceedPress = () => {
    const isSuccess = travelState.notes.trim().length > 0;
    /* const apiEndPoint = isSuccess
      ? 'https://api.npoint.io/d0fe9a5513208c354c52'
      : 'https://api.npoint.io/c5e485331b0467f4e0a9';

    fetch(apiEndPoint, { method: 'GET' })
      .then(response => response.json())
      .then(res => {
        Alert.alert(
          `Request ${isSuccess ? 'Successful' : 'Failed'}`,
          isSuccess ? `You trip id is ${res.data?.tripId}` : res.data?.message,
        );
      })
      .catch(err => console.log('flight request failed', err)); */
    Alert.alert(
      `Request ${isSuccess ? 'Successful' : 'Failed'}`,
      isSuccess
        ? 'Your trip id is QUERTY123'
        : 'Your trip is not created successfully',
    );
  };

  return (
    <>
      <StatusBar barStyle="light-content" backgroundColor="rgb(23, 27, 34)" />
      <View
        style={{ flex: 1 }}
        onLayout={event => setScreenHeight(event.nativeEvent.layout.height)}
      >
        <View
          style={[
            styles.windowBg,
            { height: screenHeight / 3, overflow: 'hidden' },
          ]}
        >
          <Image
            // @ts-expect-error: Incompatible properties
            style={[pStyles.mapBg, { height: screenHeight / 3 }]}
            source={AppImages.dotted_world_map}
            resizeMode="contain"
          />
          <View style={[styles.mapBgMask, { height: screenHeight / 3 }]} />
        </View>
        <SafeAreaView style={{ ...StyleSheet.absoluteFillObject, flex: 1 }}>
          <View style={styles.header}>
            <Image style={styles.avatar} source={AppImages.userImage} />
            <View style={{ flex: 1, marginHorizontal: 8 }}>
              <Text style={{ color: 'darkgrey' }}>Welcome Back</Text>
              <Text style={styles.username}>Ashu ! 🤟🏼</Text>
            </View>

            <View style={{ bottom: -8 }}>
              <View style={styles.fPointsContainer}>
                <View style={styles.charContainer}>
                  <Text style={{ fontWeight: 'bold', color: 'white' }}>P</Text>
                </View>
                <View style={{ gap: 4, marginEnd: 16 }}>
                  <Text style={{ fontSize: 10, color: 'darkgrey' }}>
                    Flight Point
                  </Text>
                  <View style={styles.pointsCountContainer}>
                    <Image
                      style={{ width: 10, height: 10 }}
                      source={AppImages.flight_icon}
                    />
                    <Text style={styles.pointsCount}>5,321</Text>
                  </View>
                </View>
              </View>
              <View style={styles.fPointTriangle} />
            </View>
          </View>

          <ScrollView
            contentContainerStyle={{
              padding: 16,
              paddingBottom: insets.bottom + 80,
            }}
          >
            <KeyboardAvoidingView
              style={styles.responsiveContainer}
              behavior={Config.isIos ? 'position' : undefined}
            >
              <TravelDetails />

              {travelState.selectedFlight && (
                <>
                  <Text style={styles.activeTicketTxt}>Active Ticket</Text>
                  <FlightItem
                    info={travelState.selectedFlight}
                    // isListItem={false}
                  />

                  <TextInput
                    style={[styles.inputContainer, styles.notesInput]}
                    placeholder="Notes"
                    multiline
                    numberOfLines={4}
                    value={travelState.notes}
                    onChangeText={text => dispatch(updateNotes(text))}
                  />
                </>
              )}
            </KeyboardAvoidingView>
          </ScrollView>
          <View
            style={[
              styles.proceedBtnContainer,
              {
                display: travelState.selectedFlight ? 'flex' : 'none',
                bottom: insets.bottom + 8,
              },
            ]}
          >
            <Button
              style={styles.proceedButton}
              textStyle={styles.proceedText}
              title="Proceed"
              isFill
              onPress={onProceedPress}
            />
          </View>
        </SafeAreaView>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  windowBg: {
    backgroundColor: 'rgb(23, 27, 34)',
    borderBottomLeftRadius: 16,
    borderBottomRightRadius: 16,
  },
  mapBgMask: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'rgba(23, 27, 34, 0.6)',
    width: '100%',
    zIndex: 1000,
  },
  fPointsContainer: {
    flexDirection: 'row',
    backgroundColor: 'rgba(128, 128, 128, 0.3)',
    borderRadius: 24,
    padding: 4,
    gap: 8,
  },
  pointsCountContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    gap: 4,
  },
  pointsCount: {
    color: 'white',
    fontSize: 12,
    alignItems: 'center',
  },
  fPointTriangle: {
    width: 0,
    height: 0,
    borderLeftWidth: 5,
    borderRightWidth: 5,
    borderTopWidth: 8,
    borderStyle: 'solid',
    backgroundColor: 'transparent',
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    borderTopColor: 'rgba(128, 128, 128, 0.3)',
    alignSelf: 'center',
    marginLeft: 30,
  },
  charContainer: {
    width: 30,
    height: 30,
    padding: 8,
    backgroundColor: 'rgba(128, 128, 128, 0.4)',
    borderRadius: 24,
    justifyContent: 'center',
    alignItems: 'center',
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 16,
  },
  avatar: {
    width: 40,
    height: 40,
    borderRadius: 20,
    marginHorizontal: 8,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: 'rgba(231, 236, 243, 0.4)',
  },
  username: {
    color: 'white',
    fontWeight: '600',
    fontSize: 18,
    marginTop: 4,
  },
  responsiveContainer: {
    width: '100%',
    maxWidth: 600,
    alignSelf: 'center',
  },
  inputContainer: {
    flex: 1,
    padding: 8,
  },
  proceedBtnContainer: {
    position: 'absolute',
    left: 16,
    right: 16,
    alignItems: 'center',
  },
  proceedButton: {
    width: '100%',
    maxWidth: 600,
    paddingVertical: 16,
    marginTop: 24,
    marginBottom: 8,
    elevation: 8,
    ...Platform.select({
      default: {
        shadowColor: 'rgb(82, 176, 167)',
        shadowOffset: { width: 0, height: 8 },
        shadowOpacity: 0.44,
        shadowRadius: 10.32,
      },
      web: { boxShadow: '0px 8px 10.32px rgba(82, 176, 167, 0.44)' },
    }),
  },
  proceedText: {
    color: 'white',
    textAlign: 'center',
    fontSize: 18,
    fontWeight: '600',
  },
  activeTicketTxt: {
    fontSize: 18,
    fontWeight: 'bold',
    paddingTop: 24,
    paddingBottom: 8,
  },
  notesInput: {
    minHeight: 100,
    flexDirection: 'row',
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: 'grey',
    marginTop: 16,
    padding: 12,
    borderRadius: 8,
    backgroundColor: 'white',
    ...Platform.select({
      default: { textAlignVertical: 'top' },
      web: { verticalAlign: 'top' },
    }),
  },
});

// Short for Platform Styles, seperated to avoid multiple lint error cause of web specific styles
const pStyles = StyleSheet.create({
  // @ts-expect-error: Incompatible properties
  mapBg: {
    width: '100%',
    maxWidth: 600,
    alignSelf: 'center',
    ...Platform.select({
      default: {
        transform: [
          { perspective: 1000 },
          { rotateX: '50deg' },
          { scale: 1.3 },
        ],
      },
      web: {
        transform: 'perspective(1000px) rotateX(50deg) scaleX(1.3)',
      },
    }),
  },
});

export default TravelRequest;
