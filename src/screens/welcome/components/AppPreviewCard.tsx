import React, { useRef, useState } from 'react';
import {
  View,
  Image,
  StyleSheet,
  Animated,
  useWindowDimensions,
} from 'react-native';
import { AppImages } from '../../../assets';

interface Props {
  scrollOffset: Animated.Value;
}

const AppPreviewCard: React.FC<Props> = ({ scrollOffset }) => {
  const window = useWindowDimensions();

  const [previewOffset, setPreviewOffset] = useState<number>(0);

  const previewRef = useRef<View>(null);

  const imageScale = scrollOffset.interpolate({
    inputRange: [previewOffset, previewOffset + 300],
    outputRange: [1, (120 + 48) / 120], // 48 == horizontal padding
    extrapolate: 'clamp',
  });

  return (
    <View
      style={styles.gridLeftMobilePreviewContainer}
      onLayout={_e =>
        previewRef.current?.measure((_x, _y, _w, _h, _pageX, pageY) => {
          setPreviewOffset(pageY - window.height);
        })
      }
      ref={previewRef}
    >
      <Animated.Image
        style={[styles.flightCardImg, { transform: [{ scale: imageScale }] }]}
        resizeMode="contain"
        source={AppImages.flight_item_card}
      />
      <Image
        style={styles.travelReqFormImg}
        resizeMode="contain"
        source={AppImages.travel_req_form}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  gridLeftMobilePreviewContainer: {
    flexGrow: 1,
    minWidth: '35%',
    maxHeight: 470,
    padding: 24,
    backgroundColor: 'rgb(245, 247, 250)',
    borderRadius: 16,
    margin: 24,
  },
  flightCardImg: {
    width: '100%',
    maxHeight: 120,
    alignSelf: 'center',
    aspectRatio: 1332 / 743,
    marginVertical: 24,
  },
  travelReqFormImg: {
    width: '100%',
    maxHeight: 250,
    alignSelf: 'center',
    aspectRatio: 1562 / 1745,
  },
});

export default AppPreviewCard;
