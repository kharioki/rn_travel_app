import React from 'react';
import { Image, Pressable, StyleSheet, Text, View } from 'react-native';
import { fStyles } from './styles';
import Button from '../../components/Button';
import GetIcon from '../../components/GetIcon';
import { formatTime } from '../../../util/date';
import { AppImages } from '../../../assets';
import { FlightDetail } from '../../../types';

interface FlightItemProps {
  info: FlightDetail;
  onClick?: () => void;
}

const FlightItem: React.FC<FlightItemProps> = ({ info, onClick }) => {
  const { source, destination, airlines } = info.displayData;
  const airline = airlines[0];

  return (
    <Pressable
      style={({ pressed }) => [
        styles.container,
        { opacity: pressed ? 0.6 : 1 },
      ]}
      disabled={!onClick}
      onPress={onClick}
    >
      <View style={styles.logoContainer}>
        <Image
          style={{ width: 120, height: 44 }}
          source={{ uri: airline.logo }}
          resizeMode="contain"
        />
      </View>
      <View style={styles.verticalDivider} />
      <View style={styles.middleColumn}>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Text style={fStyles.infoTextCorner} numberOfLines={1}>
            {source.airport.cityName}, {source.airport.cityCode}
          </Text>
          <Text style={fStyles.centerText}>
            {info.displayData.totalDuration}
          </Text>
          <Text
            style={[fStyles.infoTextCorner, { textAlign: 'right' }]}
            numberOfLines={1}
          >
            {destination.airport.cityName}, {destination.airport.cityCode}
          </Text>
        </View>
        <View style={styles.middleRow}>
          <Text style={fStyles.middleRowTextCorner}>
            {source.airport.airportCode}
          </Text>
          <View style={styles.flightVisualContainer}>
            <View style={fStyles.viewCircleBorder} />
            <View style={styles.flightDashRowContainer}>
              <View style={styles.dashedLine} />

              <View style={fStyles.flightIconContainer}>
                <GetIcon
                  style={fStyles.flightIcon}
                  name="airplane"
                  color="rgb(82, 176, 167)"
                  size={24}
                  type="ion"
                />
              </View>
            </View>
            <View style={fStyles.viewCircleBg} />
          </View>
          <Text style={[fStyles.middleRowTextCorner, { textAlign: 'right' }]}>
            {destination.airport.airportCode}
          </Text>
        </View>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Text style={fStyles.infoTextCorner}>
            {formatTime(source.depTime)}
          </Text>
          <Text style={fStyles.centerText}>{info.displayData.stopInfo}</Text>
          <Text style={[fStyles.infoTextCorner, { textAlign: 'right' }]}>
            {formatTime(destination.arrTime)}
          </Text>
        </View>
      </View>
      <View style={[styles.dashedLine]} />
      <View style={styles.footerRow}>
        <View style={{ justifyContent: 'space-around', alignItems: 'center' }}>
          <View style={{ alignItems: 'center', gap: 8, marginBottom: 8 }}>
            <Text style={{ color: 'grey' }}>Price per seat</Text>
            <Text style={styles.flightPrice}>
              ₹{info.fare}{' '}
              {info.originalFare && (
                <Text style={fStyles.originalPrice}>₹{info.originalFare}</Text>
              )}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', gap: 4 }}>
            <Image
              style={{ width: 18, height: 18 }}
              source={AppImages.business_icon}
              resizeMode="contain"
            />
            <Text style={{ fontSize: 16, fontWeight: '700', color: 'grey' }}>
              Business Class
            </Text>
          </View>
        </View>
        <View style={{ justifyContent: 'center' }}>
          <Button
            style={{ paddingVertical: 16 }}
            textStyle={{ fontWeight: '600' }}
            title="View Details"
            isFill
            accessibilityLabel="searchButton"
            aria-label="searchButton"
            accessibilityRole="button"
          />
          <Text style={styles.availableSeat}>
            <Text style={{ fontWeight: 'bold', color: 'black' }}>23</Text>/1200
            Available seat
          </Text>
        </View>
      </View>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    flexDirection: 'row',
    borderRadius: 16,
    margin: 8,
    overflow: 'hidden',
  },
  logoContainer: {
    flexShrink: 0.5,
    minWidth: 124,
    backgroundColor: 'white',
    padding: 16,
    justifyContent: 'center',
    alignItems: 'center',
  },
  verticalDivider: {
    width: 1,
    backgroundColor: 'rgb(231, 236, 243)',
  },
  middleColumn: {
    flexGrow: 2,
    justifyContent: 'space-between',
    backgroundColor: 'white',
    padding: 16,
    borderTopRightRadius: 16,
    borderBottomRightRadius: 16,
  },
  middleRow: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 12,
  },
  footerRow: {
    flexGrow: 1,
    flexShrink: 1,
    maxWidth: 350,
    backgroundColor: 'white',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    padding: 16,
    borderRadius: 16,
    gap: 8,
  },
  flightPrice: {
    fontSize: 24,
    fontWeight: '600',
    color: 'rgb(82, 176, 167)',
  },
  flightVisualContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  flightDashRowContainer: {
    minHeight: 28,
    flex: 1,
    justifyContent: 'center',
    marginHorizontal: 8,
  },
  dashedLine: {
    borderWidth: 1,
    borderColor: 'rgb(231, 236, 243)',
    borderStyle: 'dashed',
    marginVertical: 16,
  },
  availableSeat: {
    fontSize: 12,
    color: 'rgba(0, 0, 0, 0.3))',
    marginVertical: 8,
  },
});

export default FlightItem;
